package com.example.assignment_3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.assignment_3.Fragment.FragmentCourses;
import com.example.assignment_3.Fragment.FragmentHome;
import com.example.assignment_3.Fragment.StudentFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomNavView = findViewById(R.id.bottom);
        bottomNavView.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.Home) {
                    FragmentTransaction fragTran = getSupportFragmentManager().beginTransaction();
                    fragTran.replace(R.id.main,new FragmentHome());
                    fragTran.commit();
        } else if (item.getItemId() == R.id.Courses) {
                    FragmentTransaction fragTran = getSupportFragmentManager().beginTransaction();
                    fragTran.replace(R.id.main,new FragmentCourses());
                    fragTran.commit();
        } else if (item.getItemId() == R.id.Sections) {
                    Intent intent = new Intent(MainActivity.this, SectionAct.class);
                    startActivity(intent);
        } else if (item.getItemId() == R.id.Student) {
                    FragmentTransaction fragTran = getSupportFragmentManager().beginTransaction();
                    fragTran.replace(R.id.main,new StudentFragment());
                    fragTran.commit();
        }
            }
        });
    }

}